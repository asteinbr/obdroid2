package com.rts.obdroid2.sb;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DialRenderer;
import org.achartengine.renderer.DialRenderer.Type;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.content.Context;
import android.graphics.Color;

/**
 * Tacho Graph for Drawing Graph
 * 
 * @author S.Bajorat
 * 
 */
public class TachoGraph {

	// Graphical View using AchartEngine
	private GraphicalView view;

	// CategorySeries for giving the Graph a title
	private CategorySeries category = new CategorySeries("Speed");

	// Dial Renderer for the Graph
	private DialRenderer renderer = new DialRenderer();

	// Series Renderer for the Graph
	private SimpleSeriesRenderer r = new SimpleSeriesRenderer();

	// Constructor using WeightDialChart of AChartengine
	public TachoGraph() {
		category.add("Speed", 0);
		renderer.setChartTitleTextSize(20);
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);
		renderer.setMargins(new int[] { 20, 30, 15, 0 });
		// Set Color of the Needle
		r.setColor(Color.RED);
		renderer.addSeriesRenderer(r);
		renderer.setLabelsTextSize(10);
		// Set Color for Labels
		renderer.setLabelsColor(Color.BLACK);
		renderer.setShowLabels(true);
		// Set the Needle
		renderer.setVisualTypes(new DialRenderer.Type[] { Type.NEEDLE });
		// Set Minimum Value
		renderer.setMinValue(0);
		// Set Maximum Value
		renderer.setMaxValue(280);
		// Set Spacing between Major Ticks
		renderer.setMajorTicksSpacing(20);
		renderer.setShowLegend(false);
		// Set Spacing between Minor Ticks
		renderer.setMinorTicksSpacing(10);
	}

	// Add Points to the Graph
	public void addNewPoints(Point p) {
		category.set(0, "Speed", p.getX());
	}

	// Getter for the View of the Graph
	public GraphicalView getView(Context context) {
		view = ChartFactory.getDialChartView(context, category, renderer);
		return view;
	}
}
