package com.rts.obdroid2.sb;

import org.achartengine.GraphicalView;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Activity for the Graph with Temperature Data
 * 
 * @author S.Bajorat
 * 
 */
public class ThermoActivity extends Fragment {

	// Graphical View using AchartEngine
	private static GraphicalView view;

	// Instance of Thermo Graph
	private ThermoGraph thermo = new ThermoGraph();

	// Thread for dynamic values
	private static Thread thread;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// Get the View for Tacho Graph
		view = thermo.getView(this.getActivity());

		// Initialize Thread
		thread = new Thread() {
			// Run Thread
			public void run() {
				// Update Data
				while(true){
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					// Get Temperature Data from Receiver
					Point p = DataCollector.getThermoData();
					System.out.println("Daten: " + p.getX());
					// Add Points to Graph
					thermo.addNewPoints(p);
					// Draw the Graph
					view.repaint();
				}
			}

		};

		thread.start();
		return view;
	}
}
