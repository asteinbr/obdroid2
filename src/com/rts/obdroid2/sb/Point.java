package com.rts.obdroid2.sb;
/**
 * Point class for realizing a Point for the Graph
 * 
 * @author S.Bajorat
 * 
 */
public class Point {

	//Point x for the Graph
	private int x;

	//Constructor
	public Point(int x) {
		this.x = x;
	}

	//Getter for x
	public int getX() {
		return x;
	}

}
