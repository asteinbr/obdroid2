package com.rts.obdroid2.sb;

import java.util.Random;

import com.rts.obdroid2.EngineDataCollector;

/**
 * DataCollector is the class for collecting the Data of the Reveiver (EngineDataCollector)
 * and converting this into Point
 * 
 * @author S.Bajorat
 * 
 */

public class DataCollector {

	//Convert Speed Data into Point
	public static Point getSpeedData() {
		return new Point(getSpeed());
	}
	
	//Speed Data from Receiver
	private static int getSpeed() {
		EngineDataCollector dc = EngineDataCollector.getInstance();
		int speed = dc.getmVehicleSpeed();
		return speed;
		
	}
	
	//Convert RPM Data into Point
	public static Point getRPMData() {
		return new Point(getRPM());
	}
	
	//RPM Data from Receiver
	private static int getRPM() {
		EngineDataCollector dc = EngineDataCollector.getInstance();
		int rpm = dc.getmEngineRPM();
		return rpm;
		
	}	

	//Convert Temperature Data into Point
	public static Point getThermoData() {
		return new Point(getThermo());
	}
	
	//Temperature Data from Receiver
	private static int getThermo() {
		EngineDataCollector dc = EngineDataCollector.getInstance();
		int thermo = dc.getmEngineCoolantTemp();
		return thermo;
		
	}
	
	//Getter for Test Data
	public static Point getTestData(){
		return new Point(generateData());
	}
	
	//Generate Test Data with Random Function
	private static int generateData(){
		Random random = new Random();
		return random.nextInt(250);
	}
	
}
