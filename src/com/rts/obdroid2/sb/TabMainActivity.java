package com.rts.obdroid2.sb;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.rts.obdroid2.MainFragment;

/**
 * Class for Managing Tabs
 * 
 * @author S.Bajorat
 * 
 */
public class TabMainActivity extends Activity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Create ActionBar
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create Tab A and assign to Main Activity
		Tab tabA = actionBar.newTab();
		tabA.setText("Monitoring");
		tabA.setTabListener(new TabListener<MainFragment>(this, "Monitoring",
				MainFragment.class));
		actionBar.addTab(tabA);

		// Create Tab B and assign to Tacho Activity
		Tab tabB = actionBar.newTab();
		tabB.setText("Tacho Graph");
		tabB.setTabListener(new TabListener<TachoActivity>(this, "Tacho Graph",
				TachoActivity.class));
		actionBar.addTab(tabB);

		// Create Tab C and assign to RPM Activity
		Tab tabC = actionBar.newTab();
		tabC.setText("RPM Graph");
		tabC.setTabListener(new TabListener<RPMActivity>(this, "RPM Graph",
				RPMActivity.class));
		actionBar.addTab(tabC);

		// Create Tab D and assign Thermo Activity
		Tab tabD = actionBar.newTab();
		tabD.setText("Temperature Graph");
		tabD.setTabListener(new TabListener<ThermoActivity>(this,
				"Thermo Graph", ThermoActivity.class));
		actionBar.addTab(tabD);

		// Check if instance was created
		if (savedInstanceState != null) {
			int savedIndex = savedInstanceState.getInt("SAVED_INDEX");
			getActionBar().setSelectedNavigationItem(savedIndex);
		}

	}

	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putInt("SAVED_INDEX", getActionBar()
				.getSelectedNavigationIndex());
	}

	public static class TabListener<T extends Fragment> implements
			ActionBar.TabListener {

		private final Activity myActivity;
		private final String myTag;
		private final Class<T> myClass;

		public TabListener(Activity activity, String tag, Class<T> cls) {
			myActivity = activity;
			myTag = tag;
			myClass = cls;
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) {

			Fragment myFragment = myActivity.getFragmentManager()
					.findFragmentByTag(myTag);

			// Check if the fragment is already used
			if (myFragment == null) {
				// If not, instantiate and add it to the activity
				myFragment = Fragment
						.instantiate(myActivity, myClass.getName());
				ft.add(android.R.id.content, myFragment, myTag);
			} else {
				// If it exists, simply attach it in order to show it
				ft.attach(myFragment);
			}

		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {

			Fragment myFragment = myActivity.getFragmentManager()
					.findFragmentByTag(myTag);

			if (myFragment != null) {
				// Detach the fragment, because another one is being attached
				ft.detach(myFragment);
			}

		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			// Do nothing

		}

	}
}
