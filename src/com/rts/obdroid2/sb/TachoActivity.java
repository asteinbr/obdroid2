package com.rts.obdroid2.sb;

import org.achartengine.GraphicalView;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Activity for the Graph with Speed Data
 * 
 * @author S.Bajorat
 * 
 */
public class TachoActivity extends Fragment {

	// Graphical View using AchartEngine
	private static GraphicalView view;

	// Instance of Tacho Graph
	private TachoGraph tacho = new TachoGraph();

	// Thread for dynamic values
	private static Thread thread;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// Get the View for Tacho Graph
		view = tacho.getView(this.getActivity());

		// Initialize Thread
		thread = new Thread() {
			// Run Thread
			public void run() {
				// Update Data
				while(true){
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					// Get Speed Data from Receiver
					Point p = DataCollector.getSpeedData();
					System.out.println("Daten: " + p.getX());
					// Add Points to Graph
					tacho.addNewPoints(p);
					// Draw the Graph
					view.repaint();

				}
			}

		};

		thread.start();
		return view;
	}

}
