package com.rts.obdroid2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * BluetoothService manages the Bluetooth connection
 * 
 * Idea is from the Android Bluetooth developers guide.
 * 
 * @author asteinbr
 * 
 */
public class BluetoothService {
	// Debugging
	private static final String TAG = "BluetoothService";
	private static final boolean D = true;

	// unique uuid for the service (rfcomm spp service)
	private static final UUID MY_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");

	// Member fields
	private final BluetoothAdapter mAdapter;
	private final Handler mHandler;
	private ConnectThread mConnectThread;
	private ConnectedThread mConnectedThread;
	private int mState;

	// different connection states
	public static final int STATE_NONE = 0; // we're doing nothing
	public static final int STATE_LISTEN = 1; // now listening for incoming
												// connections
	public static final int STATE_CONNECTING = 2; // now initiating an outgoing
													// connection
	public static final int STATE_CONNECTED = 3; // now connected to a remote
													// device

	/**
	 * Constructor for BluetoothService
	 * 
	 * @param context
	 *            context of the parent activity
	 * @param handler
	 *            handler of the parent activity
	 */
	public BluetoothService(Context context, Handler handler) {
		mAdapter = BluetoothAdapter.getDefaultAdapter();
		mState = STATE_NONE;
		mHandler = handler;
	} // BluetoothService

	/**
	 * Sets state and handle it to the main activity
	 * 
	 * @param state
	 *            the new state
	 */
	private synchronized void setState(int state) {
		if (D)
			Log.d(TAG, "setState() " + mState + " -> " + state);
		mState = state;

		// handle back the new state information to ui
		mHandler.obtainMessage(MainActivity.MESSAGE_STATE_CHANGE, state, -1)
				.sendToTarget();
	} // setState

	/**
	 * Getter for state
	 * 
	 * @return delivers the current state
	 */
	public synchronized int getState() {
		return mState;
	} // getState

	/**
	 * Preparation to start the service
	 */
	public synchronized void start() {
		if (D)
			Log.d(TAG, "start()");

		// cancel any thread attempting to make a connection
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		setState(STATE_LISTEN);
	} // start

	/**
	 * Initiates the connection to a given device
	 * 
	 * @param device
	 *            the remote device
	 */
	public synchronized void connect(BluetoothDevice device) {
		if (D)
			Log.d(TAG, "connect() to: " + device);

		// cancel any thread attempting to amke a connection
		if (mState == STATE_CONNECTING) {
			if (mConnectThread != null) {
				mConnectThread.cancel();
				mConnectThread = null;
			}
		}

		// cancel already established connections
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// Starte thread for a new connection
		mConnectThread = new ConnectThread(device);
		mConnectThread.start();
		setState(STATE_CONNECTING);
	} // connect

	/**
	 * Initiates the second thread with socket and remote device to receive
	 * messages
	 * 
	 * @param socket
	 *            the socket for the bluetooth connection
	 * @param device
	 *            the remote device
	 */
	public synchronized void connected(BluetoothSocket socket,
			BluetoothDevice device) {
		if (D)
			Log.d(TAG, "connected()");

		// cancel the thread that completed the connection
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		// cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// start thread to manage the connection and perform transmissions
		mConnectedThread = new ConnectedThread(socket);
		mConnectedThread.start();

		// inform the ui
		Message msg = mHandler.obtainMessage(MainActivity.MESSAGE_DEVICE_NAME);
		Bundle bundle = new Bundle();
		bundle.putString(MainActivity.DEVICE_NAME, device.getName());
		msg.setData(bundle);
		mHandler.sendMessage(msg);

		// change state
		setState(STATE_CONNECTED);
	} // connected

	/**
	 * Stops the threads and sets the state
	 */
	public synchronized void stop() {
		if (D)
			Log.d(TAG, "stop");

		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		setState(STATE_NONE);
	} // stop

	/**
	 * Send a synchronized message to the remote device
	 */
	public void write(byte[] out) {
		// create temp object
		ConnectedThread mConnectedThreadTemp;

		// make a synchronized copy of connected thread to send the message
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			mConnectedThreadTemp = mConnectedThread;
		}

		mConnectedThreadTemp.write(out);
	} // write

	/**
	 * Constructions if the connection to the remote device fails
	 */
	private void connectionFailed() {
		// send message to ui
		Message msg = mHandler.obtainMessage(MainActivity.MESSAGE_TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(MainActivity.TOAST,
				"Unable to connect to remote device.");
		msg.setData(bundle);
		mHandler.sendMessage(msg);

		// TODO send message to log view and debug

		// start service
		this.start();
	} // connectionFailed

	/**
	 * Constructions if the connection to the remote device loses
	 */
	private void connectionLost() {
		// send message to ui
		Message msg = mHandler.obtainMessage(MainActivity.MESSAGE_TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(MainActivity.TOAST,
				"Connection lost to remote device.");
		msg.setData(bundle);
		mHandler.sendMessage(msg);

		// TODO send message to log view and debug

		// start service
		this.start();
	} // connectionLost

	/**
	 * ConnectThread
	 * 
	 * manages the outgoing connection to a device
	 * 
	 * Based on the idea of Android Bluetooth Developer Guide
	 * 
	 * @author asteinbr
	 * 
	 */
	private class ConnectThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;

		/**
		 * Constructor of ConenctThread
		 * 
		 * @param device
		 *            the remote device
		 */
		public ConnectThread(BluetoothDevice device) {
			mmDevice = device;
			BluetoothSocket mmSocketTemp = null;

			// socket
			try {
				mmSocketTemp = device
						.createRfcommSocketToServiceRecord(MY_UUID);
			} catch (IOException e) {
				Log.e(TAG,
						"Creating RfcommSocket connection (ConnectThread) failed.",
						e);
			}
			mmSocket = mmSocketTemp;
		} // ConnectThread

		/**
		 * Run method from thread
		 */
		public void run() {
			Log.i(TAG, "BEGIN mConnectThread");
			setName("ConnectThread");

			// cancel discovery
			mAdapter.cancelDiscovery();

			// create connection to socket
			try {
				mmSocket.connect();
			} catch (IOException e) {
				// try to close the socket
				try {
					mmSocket.close();
				} catch (IOException e2) {
					Log.e(TAG,
							"Unable to close() socket during connection failure.",
							e2);
				}
				connectionFailed();

				return;
			}

			// reset connected thread because here it's done
			synchronized (BluetoothService.this) {
				mConnectThread = null;
			}

			// start the connected thread
			connected(mmSocket, mmDevice);
		} // run

		/**
		 * Cancels/closes an open bluetooth socket.
		 */
		public void cancel() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed.", e);
			}
		} // cancel
	} // ConnectThread

	/**
	 * Connected Thread
	 * 
	 * manage during the connection and manage the communications of messages.
	 * 
	 * Based on the idea of Android Bluetooth Developer Guide
	 * 
	 * @author asteinbr
	 * 
	 */
	private class ConnectedThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;

		/**
		 * Constructor of ConnectedThread
		 * 
		 * @param socket
		 *            the given BluetoothSocket for the remote device
		 */
		public ConnectedThread(BluetoothSocket socket) {
			Log.d(TAG, "create ConnectedThread: ");
			mmSocket = socket;
			InputStream tempInStream = null;
			OutputStream tempOutStream = null;

			// socket
			try {
				tempInStream = socket.getInputStream();
				tempOutStream = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "Temporary for in-/output sockets not created.", e);
			}

			mmInStream = tempInStream;
			mmOutStream = tempOutStream;
		} // ConnectedThread

		/**
		 * Run method from thread
		 */
		public void run() {
			Log.i(TAG, "BEGIN mConnectedThread");

			byte[] buffer = new byte[1024];
			int bytes;

			// keep listening to inputstream while connected to receive messages
			while (true) {
				try {
					// read from inputstream
					bytes = mmInStream.read(buffer);

					// send obtained bytes to ui
					mHandler.obtainMessage(MainActivity.MESSAGE_READ, bytes,
							-1, buffer).sendToTarget();

					// sleep two seconds
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} catch (IOException e) {
					Log.e(TAG, "disconnected", e);
					connectionLost();

					// start service
					try {
						this.start();
					} catch (IllegalThreadStateException itse) {
						Log.e(TAG, "Thread was closed by remote side.", itse);
					}

					break;
				}
			} // while
		} // run

		/**
		 * Sends the message to the remote device.
		 * 
		 * @param buffer
		 *            The to be sended string in byte[] format
		 */
		public void write(byte[] buffer) {
			try {
				mmOutStream.write(buffer);

				// send outgoing message to ui
				mHandler.obtainMessage(MainActivity.MESSAGE_WRITE, -1, -1,
						buffer).sendToTarget();
			} catch (IOException e) {
				Log.e(TAG, "Exception during write ", e);
			}
		} // write

		/**
		 * Cancels/closes an open bluetooth socket.
		 */
		public void cancel() {
			// try to close the socket
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed.", e);
			}
		} // cancel
	} // ConnectedThread
}
