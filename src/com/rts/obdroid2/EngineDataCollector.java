package com.rts.obdroid2;

/**
 * This class manages the actual values of the engine.
 * 
 * To make it simple, this class will be shared as an singleton object
 * 
 * @author asteinbr
 *
 */
public class EngineDataCollector {
	int mEngineRPM = 0;
	int mEngineCoolantTemp = 0;
	int mVehicleSpeed = 0;

	private static EngineDataCollector instance;

	private EngineDataCollector() {
	}

	public synchronized static EngineDataCollector getInstance() {
		if (instance == null) {
			instance = new EngineDataCollector();
		}
		return instance;
	}

	public int getmEngineRPM() {
		return mEngineRPM;
	}

	public void setmEngineRPM(int mEngineRPM) {
		this.mEngineRPM = mEngineRPM;
	}

	public int getmEngineCoolantTemp() {
		return mEngineCoolantTemp;
	}

	public void setmEngineCoolantTemp(int mEngineCoolantTemp) {
		this.mEngineCoolantTemp = mEngineCoolantTemp;
	}

	public int getmVehicleSpeed() {
		return mVehicleSpeed;
	}

	public void setmVehicleSpeed(int mVehicleSpeed) {
		this.mVehicleSpeed = mVehicleSpeed;
	}

}
