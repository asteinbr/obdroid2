package com.rts.obdroid2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * MainActivity is the initial file for the application
 * 
 * @author asteinbr
 * 
 */
public class MainFragment extends Fragment {

	// Debug settings
	private static final String TAG = "OBDroid2";
	private static final boolean D = true;

	// Message types sent from the BluetoothService Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// Key names received from the BluetoothService Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	private static final int REQUEST_ENABLE_BT = 3;

	// Layout
	private EditText mOutMessage;
	private Button mSendButton;
	private TextView mEngineRPM;
	private TextView mEngineRPMContent;
	private TextView mEngineCoolantTemp;
	private TextView mEngineCoolantTempContent;
	private TextView mVehicleSpeed;
	private TextView mVehicleSpeedContent;
	private ListView mMessageView;

	// Engine Data
	private EngineDataCollector edc = EngineDataCollector.getInstance();

	// Timer
	private boolean monitoringStarted = false;
	private Timer timerMotorEngine;
	private Timer timerVehicleSpeed;
	private Timer timerMotorTemp;

	// Connected Device
	private String mConnectedDeviceName = null;
	// StringBuffer for outgoing messages
	private StringBuffer mOutStringBuffer = null;
	// Local Bluetooth adapter
	private BluetoothAdapter mBluetoothAdapter = null;
	// Bluetooth service
	private BluetoothService mBluetoothService = null;
	// Array Adapter which saves incomming and outgoing messages
	private ArrayAdapter<String> mMessageArrayAdapter;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (D)
			Log.e(TAG, "--> onCreate()");

		// get local bt adapter
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		// check if there is no bt adapter
		if (mBluetoothAdapter == null) {
			Toast.makeText(this.getActivity(),
					"Bluetooth is on this device not available.",
					Toast.LENGTH_LONG).show();
			getActivity().finish();

		}

		View view = inflater.inflate(R.layout.activity_main, container, false);
		setupUIWidgets(view);

		return view;
	} // onCreate

	@Override
	public void onStart() {
		super.onStart();
		if (D)
			Log.e(TAG, "--> onStart()");

		// If BT is not on, request that it be enabled.
		// setupBluetooth() will then be called during onActivityResult
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			// Otherwise, setup the chat session
		} else {
			if (mBluetoothService == null)
				setupBluetooth();
		}
	} // onStart

	@Override
	public synchronized void onResume() {
		super.onResume();
		if (D)
			Log.e(TAG, "--> onResume()");

		if (mBluetoothService != null) {
			// Only if the state is STATE_NONE, do we know that we haven't
			// started already
			if (mBluetoothService.getState() == BluetoothService.STATE_NONE) {
				// start bluetooth services
				mBluetoothService.start();
			}
		}
	} // onResume

	@Override
	public synchronized void onPause() {
		super.onPause();
		if (D)
			Log.e(TAG, "--> onPause()");
	} // onPause

	@Override
	public void onStop() {
		super.onStop();
		if (D)
			Log.e(TAG, "--> onStop()");
	} // onStop

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (D)
			Log.e(TAG, "--> onDestroy()");

		// Stop Bluetooth service
		if (mBluetoothService != null)
			mBluetoothService.stop();
	} // onDestroy

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getActivity().getMenuInflater().inflate(R.menu.activity_main, menu);
		// super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.activity_main, menu);
	} // onCreateOptionsMenu

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {

		case R.id.menu_connectThinkpad:
			if (D)
				Log.i(TAG, "menu_connectThinkpad");
			connectDevice("Thinkpad");
			return true;

		case R.id.menu_connectOBD:
			if (D)
				Log.i(TAG, "menu_connectOBD");
			connectDevice("OBD");
			return true;

		case R.id.menu_sendInitalATCommands:
			if (D)
				Log.i(TAG, "menu_sendATCommands");
			sendInitialATCommands();
			return true;

		case R.id.menu_startMonitoring:
			if (D)
				Log.i(TAG, "menu_startMonitoring");
			startMonitoring();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	} // onOptionsItemSelected

	/**
	 * Methods start monitoring the vehicle with sending in scheduled periods
	 * given PID's for engine rpm, coolant temperature and vehhicle speed.
	 */
	private void startMonitoring() {
		// if monitoring already started it will be stopped
		if (monitoringStarted) {
			timerMotorEngine.cancel();
			timerVehicleSpeed.cancel();
			timerMotorTemp.cancel();
		}

		// if monitoring not started timer tasks will start the monitoring
		if (!monitoringStarted) {
			// Motor Engine RPM
			timerMotorEngine = new Timer();
			timerMotorEngine.schedule(new TimerTask() {
				@Override
				public void run() {
					sendMessage("010C");

				}
			}, 0, 3000);

			// Vehicle Speed
			timerVehicleSpeed = new Timer();
			timerVehicleSpeed.schedule(new TimerTask() {
				@Override
				public void run() {
					sendMessage("010D");
				}
			}, 1000, 3000);

			// Motor Coolant Temperature
			timerMotorTemp = new Timer();
			timerMotorTemp.schedule(new TimerTask() {
				@Override
				public void run() {
					sendMessage("0105");
				}
			}, 2000, 3000);

			// set state to enable toggeling state
			monitoringStarted = true;
		}
	} // startMonitoring

	/**
	 * this method converts the given string to a decimal integer
	 * 
	 * @param stringHex
	 * @return
	 */
	private int convertHexToDecimal(String stringHex) {
		String mStringHex = stringHex;
		int intHex = 0;

		try {
			intHex = Integer.parseInt(mStringHex, 16);
		} catch (NumberFormatException e) {
			Toast.makeText(this.getActivity(),
					"Error by converting the received data.",
					Toast.LENGTH_SHORT).show();
		}

		return intHex;
	} // convertHexToDecimal

	/**
	 * Methods sends initial AT commands to the connected remote device
	 * 
	 * they are required to start the further communication with the remote
	 * device (OBD2)
	 */
	private void sendInitialATCommands() {
		if (D)
			Log.d(TAG, "sendInitialATCommands()");

		// the different AT commands are stored in a ArrayList
		final ArrayList<String> messagePool = new ArrayList<String>();

		messagePool.add("AT Z"); // reset all
		messagePool.add("AT DP"); // set protocol on auto

		final Iterator<String> i = messagePool.iterator();

		Timer t1 = new Timer();
		t1.schedule(new TimerTask() {
			@Override
			public void run() {
				if (i.hasNext())
					sendMessage(i.next());
			}
		}, 0, 2000);
	} // sendInitialATCommands

	/**
	 * Initializes the BluetoothService
	 */
	private void setupBluetooth() {
		Log.d(TAG, "setupBluetooth()");

		// Initialize the BluetoothService to perform bluetooth connections
		mBluetoothService = new BluetoothService(this.getActivity(), mHandler);

		// Initialize the buffer for outgoing messages
		mOutStringBuffer = new StringBuffer("");
	} // setupBluetooth

	/**
	 * Method assigns the widget of the MainActivity
	 */
	private void setupUIWidgets(final View view) {
		if (D)
			Log.d(TAG, "setupUIWidgets()");

		// ui elements
		mOutMessage = (EditText) view.findViewById(R.id.editTextOutMessage);
		mOutMessage.setOnEditorActionListener(mOutMessageListener);

		mSendButton = (Button) view.findViewById(R.id.buttonSend);
		mSendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				TextView tview = (TextView) view
						.findViewById(R.id.editTextOutMessage);
				String message = tview.getText().toString();
				sendMessage(message + "\r");

			}
		});

		// assign textview
		mEngineRPM = (TextView) view.findViewById(R.id.textView_engineRPM);
		mEngineRPMContent = (TextView) view
				.findViewById(R.id.textView_engineRPMContent);

		mEngineCoolantTemp = (TextView) view
				.findViewById(R.id.textView_engineCoolantTemp);
		mEngineCoolantTempContent = (TextView) view
				.findViewById(R.id.textView_engineCoolantTempContent);

		mVehicleSpeed = (TextView) view
				.findViewById(R.id.textView_vehicleSpeed);
		mVehicleSpeedContent = (TextView) view
				.findViewById(R.id.textView_vehicleSpeedContent);

		// mMessage = (TextView) findViewById(R.id.message);

		// Initialize the array adapter for the conversation thread
		// (The view for the message array adapter must be in a seperate file!)
		mMessageArrayAdapter = new ArrayAdapter<String>(this.getActivity(),
				R.layout.messages_view);
		mMessageView = (ListView) view.findViewById(R.id.in);
		mMessageView.setAdapter(mMessageArrayAdapter);
	} // setupUIWidgets

	/**
	 * Connects to a specific device over the options menu
	 * 
	 * @param deviceName
	 */
	private void connectDevice(String deviceName) {
		String remoteAdress = "";
		if (deviceName.contains("Thinkpad")) {
			remoteAdress = "00:1E:4C:F5:75:BB";
			// remoteAdress = "00:21:86:4B:63:E9";
		} else if (deviceName.contains("OBD")) {
			remoteAdress = "00:06:71:00:00:06";
		} else if (deviceName.contains("LianLi")) {
			remoteAdress = "00:02:5B:00:83:E6";
		}

		BluetoothDevice device = mBluetoothAdapter
				.getRemoteDevice(remoteAdress);
		mBluetoothService.connect(device);
	} // connectDevice

	/**
	 * Sends a message.
	 * 
	 * @param message
	 *            A string of text to send.
	 */
	private void sendMessage(String message) {
		// prepare message with carriage return
		message = message + "\r";

		// check bluetooth connection
		if (mBluetoothService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.makeText(this.getActivity(), R.string.not_connected,
					Toast.LENGTH_SHORT).show();
			return;
		}

		// Check that there's actually something to send
		if (message.length() > 0) {
			// Get the message bytes and tell the BluetoothService to write
			byte[] send = message.getBytes();

			// finally send the message to BluetoothService
			mBluetoothService.write(send);

			// reset stringbuffer
			mOutStringBuffer.setLength(0);
			// mOutEditText.setText(mOutStringBuffer);
		}
	} // sendMessage

	/**
	 * Connects to a remote device given by a Intent
	 * 
	 * TODO still necessary?
	 * 
	 * @param data
	 *            Intent with data
	 */
	private void connectDevice(Intent data) {
		// Get the device MAC address
		String address = data.getExtras().getString("device_address");
		// Get the BluetoothDevice object
		BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
		// Attempt to connect to the device
		mBluetoothService.connect(device);
	} // connectDevice

	/**
	 * ActionListener for the custom message textfield
	 */
	private TextView.OnEditorActionListener mOutMessageListener = new TextView.OnEditorActionListener() {
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (D)
				Log.i(TAG, "BEGIN onEditorAction");

			if (actionId == EditorInfo.IME_NULL
					&& event.getAction() == KeyEvent.ACTION_UP) {
				String message = v.getText().toString();
				sendMessage(message + "\r");
			}

			if (D)
				Log.i(TAG, "END onEditorAction");

			return true;
		} // onEditorAction
	}; // TextView.OnEditorActionListener mOutMessageListener

	/**
	 * The Handler that gets information back from the BluetoothService
	 * 
	 * TODO implementation of the state pattern possible?
	 */
	@SuppressLint("HandlerLeak")
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					// setStatus(getString(R.string.title_connected_to,
					// mConnectedDeviceName));
					mMessageArrayAdapter.clear();

					break;
				case BluetoothService.STATE_CONNECTING:
					// setStatus(R.string.title_connecting);

					break;
				case BluetoothService.STATE_LISTEN:
				case BluetoothService.STATE_NONE:
					// setStatus(R.string.title_not_connected);

					break;
				} // switch
				break;
			case MESSAGE_WRITE:
				byte[] writeBuf = (byte[]) msg.obj;
				// construct a string from the buffer
				String writeMessage = new String(writeBuf);
				mMessageArrayAdapter.add("Me: " + writeMessage);

				break;
			case MESSAGE_READ:
				byte[] readBuf = (byte[]) msg.obj;
				// construct a string from the valid bytes in the buffer
				String readMessage = new String(readBuf, 0, msg.arg1);
				mMessageArrayAdapter.add(mConnectedDeviceName + ": "
						+ readMessage);

				// ensure, that the Array Adapter is not empty and at least one
				// item inside to avoid exceptions
				if (!mMessageArrayAdapter.isEmpty()
						&& mMessageArrayAdapter.getCount() - 1 > 0) {
					String message = readMessage;
					Log.e(TAG, message);

					message = prepareResponse(message);
					int decimal = 0;

					// engine rpm
					if (message.contains("410C")) {
						message = cutResponse(message, "410C");
						decimal = convertHexToDecimal(message);
						int engineRPM = decimal / 4;

						// set value
						edc.setmEngineRPM(engineRPM);

						mEngineRPMContent.setText(String.valueOf(edc
								.getmEngineRPM()) + " rpm");
					}
					// vehicle speed
					else if (message.contains("410D")) {
						message = cutResponse(message, "410D");
						decimal = convertHexToDecimal(message);
						int vehicleSpeed = decimal;

						// set value
						edc.setmVehicleSpeed(vehicleSpeed);

						mVehicleSpeedContent.setText(String.valueOf(edc
								.getmVehicleSpeed()) + " km/h");
					}
					// engine coolant temperature
					else if (message.contains("4105")) {
						message = cutResponse(message, "4105");
						decimal = convertHexToDecimal(message);
						int engineCoolantTemp = decimal - 40;

						// set value
						edc.setmEngineCoolantTemp(engineCoolantTemp);

						mEngineCoolantTempContent.setText(String.valueOf(edc
								.getmEngineCoolantTemp()) + " C°");
					}
				} // if

				break;
			case MESSAGE_DEVICE_NAME:
				// save the connected device's name
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				Toast.makeText(getActivity(),
						"Connected to " + mConnectedDeviceName,
						Toast.LENGTH_SHORT).show();

				break;
			case MESSAGE_TOAST:
				Toast.makeText(getActivity(), msg.getData().getString(TOAST),
						Toast.LENGTH_SHORT).show();

				break;
			} // switch
		} // handleMessage
	}; // Handler mHandler

	/**
	 * Rremoves spaces and the illegal characater from given string
	 * 
	 * @param pidResponse
	 *            the string which needs to prepared like "41 0C 0F 24 >"
	 * @return the prepared string, the format will be like "410C0F24"
	 */
	protected String prepareResponse(String pidResponse) {
		String preparedString = pidResponse;

		// remove carriage return
		preparedString = preparedString.replace("\r", "");

		// remove new line
		preparedString = preparedString.replace("\n", "");

		// remove spaces
		preparedString = preparedString.replace(" ", "");

		// remove illegal character (>)
		preparedString = preparedString.replace(">", "");

		return preparedString;
	} // prepareResponse

	/**
	 * Cuts / removes a given response string the message type
	 * 
	 * @param pidResponse
	 *            String with the response
	 * @param type
	 *            Type of message as string
	 * @return The cutted response message as a string
	 */
	protected String cutResponse(String pidResponse, String type) {
		String cuttedResponse = pidResponse;
		cuttedResponse = pidResponse.replace(type, "");

		return cuttedResponse;
	} // cutResponse
} // MainActivity
